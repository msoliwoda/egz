from django.http import HttpResponse
from django.shortcuts import render
import requests
import json
from django.views.decorators.csrf import csrf_exempt


def home(request):
    import couchdb

    url = 'http://194.29.175.241:5984/calc1/_design/utils/_view/list_active'
    responsee = requests.get(url=url)
    data = json.loads(responsee.content)
    active = False
    if int(data['total_rows']) > 0:
        for row in data['rows']:
            if row['value']['host'] == 'http://msoldjango.eu01.aws.af.cm':
                active = True
        if not active:
            couch = couchdb.Server('http://194.29.175.241:5984')
            db = couch['calc1']
            doc = {'host': 'http://msoldjango.eu01.aws.af.cm', 'active': True, 'operator': '+', 'calculation': '/add'}
            db.save(doc)
    return render(request, 'index.html')

@csrf_exempt
def add(request):
    if request.method == "POST" :
        data=json.loads(request.body)
        wynik = float(data["number1"]) + float(data["number2"])
        jsonres = dict(success=True, result=float(wynik))
    else:
        jsonres = dict(success=False)

    return HttpResponse(content = json.dumps(jsonres), content_type="application/json")

@csrf_exempt
def onp(request):
    if(request.method == 'POST'):
        text = request.POST['onp'];
        text = text.split(' ')
        stos = []

        try:
            for i in text:
                try:
                    stos.append(int(i))
                except ValueError:
                    if i == '+':
                        stos.append(stos.pop(len(stos) - 2) + stos.pop(len(stos) - 1))
                    elif i == '-':
                        stos.append(stos.pop(len(stos) - 2) - stos.pop(len(stos) - 1))
                    elif i == '*':
                        stos.append(stos.pop(len(stos) - 2) * stos.pop(len(stos) - 1))
                    elif i == '/':
                        stos.append(stos.pop(len(stos) - 2) / stos.pop(len(stos) - 1))
        except IndexError:
            pass
        return HttpResponse(content = json.dumps(stos), content_type="application/json")
    else:
        return HttpResponse('Blad');

def get_servers_list(request):
    list = requests.get('http://194.29.175.241:5984/calc1/_design/utils/_view/list_active').json()
    data = [];
    for row in list['rows']:
        if row['value']['active']:
            data.append({'name': row['value']['host'], 'op': row['value']['operator'], 'calc' : row['value']['calculation']})
    return HttpResponse(content = json.dumps(data), content_type="application/json")